@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <div class="">
                <a><img src="{{ URL::asset('images/new.png') }}" style="width: 100px;height: 30px;">
                   <label style="font-size: 20px">URL Shortener</label>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="inner-container">
        <div style="background-color: #366ed1;padding: 30px 0 150px;margin-top: 20px">
                <div class="col-md-2 col-xs-2 col-lg-2"></div>
                <div class="col-md-6 col-xs-6 col-lg-6">
                    <label style="font-size: 25px;color: white">Simplify Your links</label>
                    <input type="text" name="long_link" id="long_link" class="form-control" placeholder="Your original URL here">
                    <label style="color: white">All goo.gl URLs and click analytics are public and can be accessed by anyone</label>
                </div>
                <div class="col-md-4 col-xs-4 col-lg-4">
                    <input type="submit" name="submit" value="SHORTEN URL" class="btn" style="margin-top: 43px;" onclick="makeShort()">
                </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-6">
                <div id="popUpDiv"></div>
            </div>
        </div>
</div>
@endsection

<script>
function makeShort()
{
    var long_link = $("#long_link").val();
    
    $.ajax({
            type:'post',
            url: "{{ url('collectUrl') }}", 
            data:{"long_link":long_link},
            success: function(result){
            $("#popUpDiv").html(result);
    }});
}
</script>