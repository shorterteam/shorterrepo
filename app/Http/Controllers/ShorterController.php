<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShorterController extends Controller
{
    //taking long url and making into short url

    public function getUrl()
    {
    	$long_link = $("#long_link").val();
    	$shortObject = new ShorterObject;
    	$randomString = $this->generateRandomString;
    	$shortObject->long = $long_link;
    	$shortObject->shortUrl= "http://goo.gl.com".$randomString;

    	$output = $shortObject->save();

    	if($output)
    	{
    		$resultData = ShorterObject::all();
    		foreach($resultData as $r)
    		{
    			$result = 'Actual Url'.$r->long.', Short Url'.$r->shortUrl;
    		}

    		print_r($result);
    	}

    }

	//for random string generation
    function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

}